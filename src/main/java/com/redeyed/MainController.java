package com.redeyed;

import com.redeyed.config.Config;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainController {

    public static ApplicationContext ctx;

    public MainController() {
    }



    public static void main(String[] args) {
        ctx = new AnnotationConfigApplicationContext(Config.class);
//        new DatabaseConnector();
    }
}

package com.redeyed.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.activation.DataSource;

@Component
public class DataSourceImpl implements com.redeyed.repository.DataSource {
    private final DataSource dataSource;

    @Autowired
    public DataSourceImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        hello();
    }

    @Override
    public void hello() {
        System.out.println("Hello from DataSource");
    }

    // ...
}
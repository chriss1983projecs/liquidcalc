package com.redeyed.annotations;

import org.reflections.Reflections;

import java.util.Set;

public class AnnotationManager {
    public static AnnotationManager instance = new AnnotationManager();

    private AnnotationManager() { }

    public static Set<Class<?>> getAnnotatedClassed(String packageForSearch) {
        if(instance == null) {
            instance = new AnnotationManager();
        }
        return new Reflections(packageForSearch).getTypesAnnotatedWith(Model.class);
    }
}

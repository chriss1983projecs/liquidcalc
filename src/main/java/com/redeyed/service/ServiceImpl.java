package com.redeyed.service;

import com.redeyed.repository.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements com.redeyed.service.Service {
    private final DataSource fooRepository;

    @Autowired
    public ServiceImpl(DataSource fooRepository) {
        this.fooRepository = fooRepository;
    }

    @Override
    public void hello() {
        System.out.println("Hello from Service");
    }
}
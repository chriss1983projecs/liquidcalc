package com.redeyed.database;

import com.redeyed.annotations.Model;
import com.redeyed.model.Group;
import com.redeyed.model.User;
import com.redeyed.pojos.Handler;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;

public class AnnotationTest {
    final static Logger logger = Logger.getLogger(AnnotationTest.class);

    private String PACKAGE = "com.redeyed";

    Reflections reflections;
    Set<Class<?>> annotated;

    @Before
    public void prepare() {
        reflections = new Reflections(PACKAGE);
        annotated = reflections.getTypesAnnotatedWith(Model.class);
    }

    @Test
    public void testAnnotations() {
        for (Class<?> controller : annotated) {
            logger.debug(controller.getAnnotatedSuperclass().getClass().getSimpleName());
            logger.debug(controller);

        }
    }

    @Test
    public void checkIfContainsObjects() {
        Assert.assertTrue(annotated.contains(Group.class));
//        Assert.assertTrue(annotated.contains(Player.class));
        Assert.assertTrue(annotated.contains(User.class));
        Assert.assertTrue(annotated.contains(Handler.class));
    }
}
